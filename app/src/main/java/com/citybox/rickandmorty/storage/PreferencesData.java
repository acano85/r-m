package com.citybox.rickandmorty.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.citybox.rickandmorty.network.model.episodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class PreferencesData {

    private static final String STORAGE = "storage";
    private static final String EPISODES_LIST = "episodes_list";

    private final SharedPreferences pref;
    private final SharedPreferences.Editor editor;
    private  List<episodes.EpisodesResult> list;

    public PreferencesData(Context context) {
        list= new ArrayList<>();
        pref = context.getApplicationContext().getSharedPreferences(STORAGE, MODE_PRIVATE);
        editor = pref.edit();
    }

    public void saveEpisodesList(List<episodes.EpisodesResult> episodesList){
          list=episodesList;
          Gson gson = new Gson();
          String jsonList = gson.toJson(episodesList);
          editor.putString(EPISODES_LIST,jsonList);
          editor.commit();
  }


  public List<episodes.EpisodesResult>  getEpisodesList(){
          Gson gson = new Gson();
          String jsonList=pref.getString(EPISODES_LIST, null);
          Type type = new TypeToken<List<episodes.EpisodesResult>>(){}.getType();
          List<episodes.EpisodesResult>  list = gson.fromJson(jsonList, type);
          return list;
  }
}
