package com.citybox.rickandmorty.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class episodes {

    //JSON STRUCTURE

//    {
//        "info": {
//        "count": 31,
//                "pages": 2,
//                "next": "https://rickandmortyapi.com/api/episode?page=2",
//                "prev": ""
//    },
//        "results": [
//        {
//            "id": 1,
//                "name": "Pilot",
//                "air_date": "December 2, 2013",
//                "episode": "S01E01",
//                "characters": [
//            "https://rickandmortyapi.com/api/character/1",
//                    "https://rickandmortyapi.com/api/character/2",
//                    "https://rickandmortyapi.com/api/character/35",
//                    "https://rickandmortyapi.com/api/character/38",
//                    "https://rickandmortyapi.com/api/character/62",
//                    "https://rickandmortyapi.com/api/character/92",
//                    "https://rickandmortyapi.com/api/character/127",
//                    "https://rickandmortyapi.com/api/character/144",
//                    "https://rickandmortyapi.com/api/character/158",
//                    "https://rickandmortyapi.com/api/character/175",
//                    "https://rickandmortyapi.com/api/character/179",
//                    "https://rickandmortyapi.com/api/character/181",
//                    "https://rickandmortyapi.com/api/character/239",
//                    "https://rickandmortyapi.com/api/character/249",
//                    "https://rickandmortyapi.com/api/character/271",
//                    "https://rickandmortyapi.com/api/character/338",
//                    "https://rickandmortyapi.com/api/character/394",
//                    "https://rickandmortyapi.com/api/character/395",
//                    "https://rickandmortyapi.com/api/character/435"
//            ],
//            "url": "https://rickandmortyapi.com/api/episode/1",
//                "created": "2017-11-10T12:56:33.798Z"
//        },

    @SerializedName("results")
    List <EpisodesResult> episodesResults;

    public List<EpisodesResult> getEpisodesResults() {
        return episodesResults;
    }

    public class EpisodesResult implements Parcelable {
        @SerializedName("id")
        Integer id;
        @SerializedName("name")
        String name;
        @SerializedName("air_date")
        String air_date;
        @SerializedName("episode")
        String episode;
        @SerializedName("characters")
        List<String> characters;
        @SerializedName("url")
        String url;

        protected EpisodesResult(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            name = in.readString();
            air_date = in.readString();
            episode = in.readString();
            characters = in.createStringArrayList();
            url = in.readString();
        }

        public  final Creator<EpisodesResult> CREATOR = new Creator<EpisodesResult>() {
            @Override
            public EpisodesResult createFromParcel(Parcel in) {
                return new EpisodesResult(in);
            }

            @Override
            public EpisodesResult[] newArray(int size) {
                return new EpisodesResult[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getAir_date() {
            return air_date;
        }

        public String getEpisode() {
            return episode;
        }

        public List<String> getCharacters() {
            return characters;
        }

        public String getUrl() {
            return url;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(name);
            dest.writeString(air_date);
            dest.writeString(episode);
            dest.writeStringList(characters);
            dest.writeString(url);
        }
    }
}
