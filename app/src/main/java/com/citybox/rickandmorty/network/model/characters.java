package com.citybox.rickandmorty.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class characters implements Parcelable {


        @SerializedName("id")
        Integer id;
        @SerializedName("name")
        String name;
        @SerializedName("status")
        String status;
        @SerializedName("species")
        String species;
        @SerializedName("type")
        String type;
        @SerializedName("gender")
        String gender;
        @SerializedName("image")
        String image;
        @SerializedName("url")
        String url;
        @SerializedName("origin")
        Origin origin;
        @SerializedName("location")
        Location location;
        @SerializedName("episode")
        List<String> episode;

    protected characters(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        status = in.readString();
        species = in.readString();
        type = in.readString();
        gender = in.readString();
        image = in.readString();
        url = in.readString();
        episode = in.createStringArrayList();
    }

    public static final Creator<characters> CREATOR = new Creator<characters>() {
        @Override
        public characters createFromParcel(Parcel in) {
            return new characters(in);
        }

        @Override
        public characters[] newArray(int size) {
            return new characters[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(status);
        dest.writeString(species);
        dest.writeString(type);
        dest.writeString(gender);
        dest.writeString(image);
        dest.writeString(url);
        dest.writeStringList(episode);
    }

    class Origin {

            @SerializedName("name")
            String originName;
            @SerializedName("url")
            String originUrl;

            public String getOriginName() {
                return originName;
            }

            public String getOriginUrl() {
                return originUrl;
            }
        }

        class Location {
            @SerializedName("name")
            String locationName;
            @SerializedName("url")
            String locationUrl;

            public String getLocationName() {
                return locationName;
            }

            public String getLocationUrl() {
                return locationUrl;
            }
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getStatus() {
            return status;
        }

        public String getSpecies() {
            return species;
        }

        public String getType() {
            return type;
        }

        public String getGender() {
            return gender;
        }

        public Origin getOrigin() {
            return origin;
        }

        public Location getLocation() {
            return location;
        }

        public String getImage() {
            return image;
        }

        public List<String> getEpisode() {
            return episode;
        }

        public String getUrl() {
            return url;
        }


}