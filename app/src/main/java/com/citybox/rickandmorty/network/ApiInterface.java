package com.citybox.rickandmorty.network;


import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface ApiInterface {

    @GET("api/episode?")
    Call<episodes> getEpisodes(@Query("page") Integer page);


    @GET("api/character/{ids}")
    Call <List<characters>>getCharacters(@Path("ids") String ids);

}
