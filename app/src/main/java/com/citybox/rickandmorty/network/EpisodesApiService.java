package com.citybox.rickandmorty.network;

import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EpisodesApiService {


    private final ApiInterface apiInterface;
    private final episodesInterface callback;

    public EpisodesApiService(episodesInterface callback) {
        this.callback=callback;
        apiInterface = APIClient.getClient().create(ApiInterface.class);

    }

    public void getEspisodes(Integer page){


        Call<episodes> call = apiInterface.getEpisodes(page);
        call.enqueue(new Callback<episodes>() {
            @Override
            public void onResponse(Call<episodes> call, Response<episodes> response) {
                if(response.body()!=null){
                    callback.getEpisodesList(response.body().getEpisodesResults());
                }

            }

            @Override
            public void onFailure(Call<episodes> call, Throwable t) {
                call.cancel();
                callback.getError("Connection issue");
            }
        });

    }

    public interface episodesInterface{
        void getEpisodesList(List<episodes.EpisodesResult> episodesList);

        void getError(String error);
    }
}
