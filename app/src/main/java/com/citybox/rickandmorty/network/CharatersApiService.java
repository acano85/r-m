package com.citybox.rickandmorty.network;

import android.util.Log;

import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharatersApiService {


    private final ApiInterface apiInterface;
    private final charactersInterface callback;

    public CharatersApiService(charactersInterface callback) {
        apiInterface = APIClient.getClient().create(ApiInterface.class);
        this.callback=callback;

    }

    public void getCharacters(String ids){


        Call<List<characters>> call = apiInterface.getCharacters(ids);
        call.enqueue(new Callback<List<characters>> () {
            @Override
            public void onResponse(Call<List<characters>>  call, Response<List<characters>> response) {

                if(response.body()!=null){
                    callback.getCharactersList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<characters>>  call, Throwable t) {
                call.cancel();
                    callback.getError("Connection issue");

            }
        });

    }

    public interface charactersInterface{
        void getCharactersList( List<characters>charactersList);

        void getError(String error);
    }
}
