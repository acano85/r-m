package com.citybox.rickandmorty.main;

public class Assembly {
    private final AssemblyInterface view;

    public Assembly(AssemblyInterface view) {
            this.view=view;
    }

    public void start() {
            view.loadListFragment();
    }

    public void loadDetail() {
        view.loadDetailFragment();
    }
}
