package com.citybox.rickandmorty.main;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.citybox.rickandmorty.R;
import com.citybox.rickandmorty.main.episode_detail.EpisodeDatailFragment;
import com.citybox.rickandmorty.main.episodes.EpisodesFragment;
import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;

public class MainActivity extends AppCompatActivity implements AssemblyInterface, EpisodesFragment.OnLeaveListListener, EpisodeDatailFragment.OnFragmentInteractionListener {

    private Assembly assembly;
    private episodes.EpisodesResult episodeSelected;
    private List<characters> characterList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assembly = new Assembly(this);
        setContentView(R.layout.activity_main_layout);

        assembly.start();


    }

    /************************************************************************
     * ASSEMBLY CALLBACKS
     *************************************************************************/


    @Override
    public void loadListFragment() {
        EpisodesFragment episodesFragment = EpisodesFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, episodesFragment);
        ft.commit();    }

    @Override
    public void loadDetailFragment() {
        EpisodeDatailFragment episodeDatailFragment = EpisodeDatailFragment.newInstance(episodeSelected,characterList);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, episodeDatailFragment);
        ft.addToBackStack(null);
        ft.commit();       }

    /************************************************************************
     * FRAGMENT CALLBACKS
     *************************************************************************/


    @Override
    public void loadEpisodeDetail(episodes.EpisodesResult episodeSelected, List<characters> charactersList) {
        this.episodeSelected=episodeSelected;
        this.characterList=charactersList;
        assembly.loadDetail();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
