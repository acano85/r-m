package com.citybox.rickandmorty.main.episode_detail;

import com.citybox.rickandmorty.network.model.characters;

import java.util.List;

public interface EpisodeDetailView {

    void setFilteredList(List<characters> filteredList);
}
