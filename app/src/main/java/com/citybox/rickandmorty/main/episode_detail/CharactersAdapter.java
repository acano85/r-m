package com.citybox.rickandmorty.main.episode_detail;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.citybox.rickandmorty.R;
import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.MyViewHolder> {
    private final Context context;
    private List<characters> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewType;
        TextView textViewStatus;
        ImageView imageView;
        CardView cardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.cardView = (CardView) itemView.findViewById(R.id.card_view);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.textViewType = (TextView) itemView.findViewById(R.id.type);
            this.textViewStatus = (TextView) itemView.findViewById(R.id.status);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);

        }
    }

    public CharactersAdapter(List<characters> data, Context context) {
        this.dataSet = data;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.character_layout, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewType = holder.textViewType;
        TextView textViewStatus = holder.textViewStatus;
        ImageView imageView = holder.imageView;

        textViewName.setText(dataSet.get(listPosition).getName());
        textViewType.setText(dataSet.get(listPosition).getSpecies());
        textViewStatus.setText(dataSet.get(listPosition).getStatus());


        Picasso.get()
                .load(dataSet.get(listPosition).getImage())
                .error(R.drawable.ic_launcher_foreground)
                .into(imageView);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}