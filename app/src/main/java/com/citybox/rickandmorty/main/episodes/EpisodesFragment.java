package com.citybox.rickandmorty.main.episodes;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.citybox.rickandmorty.R;
import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;


public class EpisodesFragment extends Fragment implements EpisodesView, EpisodesAdapter.OnItemClickListener {

    private static RecyclerView recyclerViewT1;
    private static RecyclerView recyclerViewT2;
    private static RecyclerView recyclerViewT3;


    private static List<episodes.EpisodesResult>  data;
    private OnLeaveListListener mListener;
    private View view;
    private EpisodesPresenterImp episodesPresenterImp;

    public EpisodesFragment() {}

    public static EpisodesFragment newInstance() {
        EpisodesFragment fragment = new EpisodesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =inflater.inflate(R.layout.fragment_list, container, false);
        setupRecyclers();
        episodesPresenterImp= new EpisodesPresenterImp(this,getActivity());
        episodesPresenterImp.userEnter();
        return view;
    }


    private void setupRecyclers() {

        recyclerViewT1 = (RecyclerView) view.findViewById(R.id.my_recycler_view_t1);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewT1.setLayoutManager(horizontalLayoutManagaer);
        recyclerViewT1.setItemAnimator(new DefaultItemAnimator());

        recyclerViewT2 = (RecyclerView) view.findViewById(R.id.my_recycler_view_t2);
        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewT2.setLayoutManager(horizontalLayoutManagaer2);
        recyclerViewT2.setItemAnimator(new DefaultItemAnimator());

        recyclerViewT3 = (RecyclerView) view.findViewById(R.id.my_recycler_view_t3);
        LinearLayoutManager horizontalLayoutManagaer3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewT3.setLayoutManager(horizontalLayoutManagaer3);
        recyclerViewT3.setItemAnimator(new DefaultItemAnimator());

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLeaveListListener) {
            mListener = (OnLeaveListListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLeaveListListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(episodes.EpisodesResult item) {
        episodesPresenterImp.userClick(item);
    }


    public interface OnLeaveListListener {
        void loadEpisodeDetail(episodes.EpisodesResult episodeSelected, List<characters> charactersList);
    }





    /**********************************************************
     * CALLBAKCS
     * @param message
     ***********************************************************/
    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getEpisodesListT1(List<episodes.EpisodesResult> episodesList) {
        EpisodesAdapter adapter = new EpisodesAdapter(episodesList,this);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewT1);
        recyclerViewT1.setAdapter(adapter);
    }

    @Override
    public void getEpisodesListT2(List<episodes.EpisodesResult> episodesList) {
        EpisodesAdapter adapter = new EpisodesAdapter(episodesList,this);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewT2);
        recyclerViewT2.setAdapter(adapter);
    }

    @Override
    public void getEpisodesListT3(List<episodes.EpisodesResult> episodesList) {
        EpisodesAdapter adapter = new EpisodesAdapter(episodesList,this);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewT3);
        recyclerViewT3.setAdapter(adapter);
    }

    @Override
    public void goToEpisodeDetail(episodes.EpisodesResult episodeSelected, List<characters> charactersList) {
        mListener.loadEpisodeDetail(episodeSelected,charactersList);
    }


}
