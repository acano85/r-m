package com.citybox.rickandmorty.main.episode_detail;

import com.citybox.rickandmorty.network.model.characters;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import static android.text.util.Linkify.ALL;

public class EpisodeDetailPresenterImp implements EpisodeDetailPresenter{

    private static final int ALL_CHARACTERS =0;
    private static final int ALIVE =1;
    private static final int DEAD =2;
    private static final int UNKNOWN =3;
    private static final String ALIVE_KEY ="Alive";
    private static final String DEAD_KEY ="Dead";
    private static final String UNKNOWN_KEY ="unknown";
    private final EpisodeDetailView view;
    private final List<characters> defaultList;
    private AbstractList<characters> aliveList;
    private AbstractList<characters> deadList;
    private AbstractList<characters> unknownList;

    public EpisodeDetailPresenterImp(EpisodeDetailView view, List<characters> list) {
        aliveList= new ArrayList<>();
        deadList= new ArrayList<>();
        unknownList= new ArrayList<>();

        this.defaultList=list;
        this.view=view;
        filterAction();
    }

    private void filterAction() {

        for(characters character:defaultList){
            switch (character.getStatus()){
                case ALIVE_KEY:{
                    aliveList.add(character);
                }
                break;
                case DEAD_KEY:{
                    deadList.add(character);
                }
                break;
                case UNKNOWN_KEY:{
                    unknownList.add(character);
                }
                break;
            }
        }
    }

    /****
     *Filter method
     * @param selection
     * FLAGS:
    //0=ALL//1=ALIVE//2=DEAD//3=UNKNOWN
     */
    @Override
    public void filter(int selection) {

        switch (selection){
            case ALL_CHARACTERS:{
                view.setFilteredList(defaultList);
            }
            break;
            case ALIVE:{
                view.setFilteredList(aliveList);
            }
            break;
            case DEAD:{
                view.setFilteredList(deadList);

            }
            break;
            case UNKNOWN:{
                view.setFilteredList(unknownList);
            }
            break;
        }

    }
}
