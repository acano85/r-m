package com.citybox.rickandmorty.main.episodes;

import android.content.Context;
import android.os.IInterface;

import com.citybox.rickandmorty.network.CharatersApiService;
import com.citybox.rickandmorty.network.EpisodesApiService;
import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;
import com.citybox.rickandmorty.storage.PreferencesData;

import java.util.ArrayList;
import java.util.List;

public class EpisodesPresenterImp implements  EpisodesPresenter, EpisodesApiService.episodesInterface, CharatersApiService.charactersInterface {


    private static final CharSequence S01 = "S01";
    private static final CharSequence S02 = "S02";
    private static final CharSequence S03 = "S03";

    private static Integer PAGE_ONE = 1;
    private final EpisodesView view;
    private final Context context;
    private Integer page;
    private List<episodes.EpisodesResult> list;
    private EpisodesApiService episodesApiService;
    private episodes.EpisodesResult episodeSelected;

    public EpisodesPresenterImp(EpisodesView view, Context context) {
        this.context=context;
        this.view=view;
    }

    @Override
    public void userEnter() {

        PreferencesData preferencesData= new PreferencesData(context);
        list=preferencesData.getEpisodesList();

        if(list!=null){
            sortList();
        }
        else {
            list= new ArrayList<>();
            page=PAGE_ONE;
            episodesApiService= new EpisodesApiService(this);
            episodesApiService.getEspisodes(page);
        }

    }

    @Override
    public void userClick(episodes.EpisodesResult item) {
        episodeSelected=item;
        List<String> charactersList = item.getCharacters();
        ArrayList<String> listStrings= new ArrayList<>();
        String idStringList="";
        for(String chararcter:charactersList){
            String[] arr = chararcter.split("/");

            listStrings.add(arr[arr.length-1]);
        }
        CharatersApiService charatersApiService= new CharatersApiService(this);
        charatersApiService.getCharacters(listStrings.toString());
    }

    /**************************************************
     * API SERVICE CALLBACKS
     * @param episodesList
     ***************************************************/
    @Override
    public void getEpisodesList(List<episodes.EpisodesResult> episodesList) {

        if(page==PAGE_ONE){
            list.addAll(episodesList);
            page++;
            episodesApiService.getEspisodes(page);

        }else {
            list.addAll(episodesList.size(),episodesList);
            saveList();
            sortList();
        }

    }

    @Override
    public void getCharactersList(List<characters> charactersList) {
            view.goToEpisodeDetail(episodeSelected,charactersList);
    }

    @Override
    public void getError(String error) {
        view.showError(error);
    }

    /**************************************************
     * LOGIC METHODS
     ***************************************************/
    private void saveList() {
        PreferencesData preferencesData= new PreferencesData(context);
        preferencesData.saveEpisodesList(list);
    }

    private void sortList() {

        List<episodes.EpisodesResult> listT1= new ArrayList<>();
        List<episodes.EpisodesResult> listT2= new ArrayList<>();
        List<episodes.EpisodesResult> listT3= new ArrayList<>();


        for(episodes.EpisodesResult episode: list){
            if(episode.getEpisode().contains(S01)){
                listT1.add(episode);
            }
            else if((episode.getEpisode().contains(S02))){
                listT2.add(episode);
            }
            else if((episode.getEpisode().contains(S03))){
                listT3.add(episode);
            }
        }

        view.getEpisodesListT1(listT1);
        episodes.EpisodesResult ep=listT2.get(0);
        listT2.remove(listT2.get(0));
        listT2.add(listT2.size(),ep);
        view.getEpisodesListT2(listT2);
        view.getEpisodesListT3(listT3);


    }

}
