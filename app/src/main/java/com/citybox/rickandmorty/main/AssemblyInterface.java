package com.citybox.rickandmorty.main;

public interface AssemblyInterface {

    void loadListFragment();

    void loadDetailFragment();
}
