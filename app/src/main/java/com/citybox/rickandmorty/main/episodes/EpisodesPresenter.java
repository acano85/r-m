package com.citybox.rickandmorty.main.episodes;

import com.citybox.rickandmorty.network.model.episodes;

public interface EpisodesPresenter {

    void userEnter();

    void userClick(episodes.EpisodesResult item);
}
