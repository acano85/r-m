package com.citybox.rickandmorty.main.episode_detail;

import com.citybox.rickandmorty.network.model.characters;

import java.util.List;

public interface EpisodeDetailPresenter {

    void filter(int selection);
}
