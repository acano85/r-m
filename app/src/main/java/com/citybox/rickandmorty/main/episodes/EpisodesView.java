package com.citybox.rickandmorty.main.episodes;

import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.List;

public interface EpisodesView {

    void showError(String message);

    void getEpisodesListT1(List<episodes.EpisodesResult> episodesList);
    void getEpisodesListT2(List<episodes.EpisodesResult> episodesList);
    void getEpisodesListT3(List<episodes.EpisodesResult> episodesList);

    void goToEpisodeDetail(episodes.EpisodesResult episodeSelected, List<characters> charactersList);
}
