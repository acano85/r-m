package com.citybox.rickandmorty.main.episode_detail;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.citybox.rickandmorty.R;
import com.citybox.rickandmorty.network.model.characters;
import com.citybox.rickandmorty.network.model.episodes;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EpisodeDatailFragment extends Fragment implements EpisodeDetailView{


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.code)
    TextView code;
    @BindView(R.id.rvCharacters)
    RecyclerView rvCharacters;

    private static final String EPISODE_RESULT = "episode_result";
    private static final String CHARACTERS_LIST = "characters_list";
    Unbinder unbinder;
    @BindView(R.id.filterButton)
    ImageButton filterButton;
    private episodes.EpisodesResult episodesResult;
    private List<characters> charactersList;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;
    private int selectedSpecies=0;
    private EpisodeDetailPresenterImp episodeDetailPresenter;
    private CharactersAdapter adapter;
    private RecyclerView recyclerView;

    public EpisodeDatailFragment() {
    }

    /**
     * @param param1 episodes.EpisodesResult
     * @param param2 List<characters>
     * @return A new instance of fragment EpisodeDatailFragment.
     */
    public static EpisodeDatailFragment newInstance(episodes.EpisodesResult param1, List<characters> param2) {
        EpisodeDatailFragment fragment = new EpisodeDatailFragment();
        Bundle args = new Bundle();
        args.putParcelable(EPISODE_RESULT, param1);
        args.putParcelableArrayList(CHARACTERS_LIST, (ArrayList<? extends Parcelable>) param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            episodesResult = getArguments().getParcelable(EPISODE_RESULT);
            charactersList = getArguments().getParcelableArrayList(CHARACTERS_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_episode_datail, container, false);
        unbinder = ButterKnife.bind(this, view);
        episodeDetailPresenter= new EpisodeDetailPresenterImp(this,charactersList);
        setupFields();
        return view;
    }

    private void setupFields() {
        title.setText(episodesResult.getName());
        code.setText(episodesResult.getEpisode());

        recyclerView = (RecyclerView) view.findViewById(R.id.rvCharacters);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

         adapter = new CharactersAdapter(charactersList, getActivity());
        recyclerView.setAdapter(adapter);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.filterButton)
    public void onFilterButtonClicked() {
        showPopUpFilterOptions();
    }

    private void showPopUpFilterOptions() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        CharSequence items[] = new CharSequence[] {getResources().getString(R.string.status_all),getResources().getString(R.string.status_alive), getResources().getString(R.string.status_dead), getResources().getString(R.string.status_unknown)};
        adb.setSingleChoiceItems(items, selectedSpecies, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface d, int n) {
                selectedSpecies=n;
                episodeDetailPresenter.filter(selectedSpecies);
                d.dismiss();
            }

        });
        adb.setTitle(getResources().getString(R.string.select_status));
        adb.show();
    }

    @Override
    public void setFilteredList(List<characters> filteredList) {

        adapter = new CharactersAdapter(filteredList, getActivity());
        recyclerView.setAdapter(adapter);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
