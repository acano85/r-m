package com.citybox.rickandmorty;

import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
public class EpisodeDetailPresenterImpTest {



    @Test
    public void filterAction() throws Exception {
        ArrayList<String> list= new ArrayList<>();
        list.add("Unknown");
        list.add("Alive");
        list.add("Unknown");
        list.add("Alive");
        list.add("Alive");
        list.add("Dead");
        list.add("Alive");
        list.add("Dead");
        list.add("Alive");
        list.add("Unknown");

        ArrayList<String> aliveList = new ArrayList<>();

        //0=ALL//1=ALIVE//2=DEAD//3=UNKNOWN
        aliveList=getListOfStatus(list,1);
        ArrayList<String> deadList = getListOfStatus(list, 2);
        ArrayList<String> unknownList = getListOfStatus(list, 3);
        ArrayList<String> defaultList = getListOfStatus(list, 0);


            assertEquals(10, list.size());
            assertEquals(5, aliveList.size());
            assertEquals(2, deadList.size());
            assertEquals(3, unknownList.size());

        }

    private ArrayList<String> getListOfStatus(ArrayList<String> list, int status) {
        ArrayList<String> aliveList = new ArrayList<>();
        ArrayList<String> deadList= new ArrayList<>();
        ArrayList<String> unknwnList= new ArrayList<>();


        for (String str: list){
            if(str.equals("Alive")){
                aliveList.add(str);
            }
            else if(str.equals("Dead")){
                deadList.add(str);
            }
            else if(str.equals("Unknown")){
                unknwnList.add(str);
            }
         }
        //0=ALL//1=ALIVE//2=DEAD//3=UNKNOWN

        if(status==1){
            return aliveList;
        }

        else if(status==2){
            return deadList;

        }
        else if(status==3){
            return unknwnList;
        }
        else {
            return list;
        }
    }
}